﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [dbo].[EnterpriseTypes] (
    [Id] int NOT NULL IDENTITY,
    [Active] bit NOT NULL,
    [Name] varchar(50) NOT NULL,
    CONSTRAINT [PK_EnterpriseTypes] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [dbo].[Enterprises] (
    [Id] bigint NOT NULL IDENTITY,
    [Active] bit NOT NULL,
    [Name] varchar(50) NOT NULL,
    [Description] nvarchar(max) NOT NULL,
    [Value] bigint NOT NULL DEFAULT CAST(0 AS bigint),
    [City] varchar(50) NOT NULL,
    [Country] varchar(70) NOT NULL,
    [Email] varchar(100) NULL,
    [Facebook] varchar(200) NULL,
    [Twitter] varchar(100) NULL,
    [Linkedin] varchar(200) NULL,
    [Phone] varchar(14) NULL,
    [Photo] nvarchar(max) NULL,
    [Shares] bigint NOT NULL,
    [SharePrice] bigint NOT NULL,
    [OwnShares] bigint NOT NULL,
    [OwnEnterprise] bit NOT NULL DEFAULT 0,
    [EnterpriseTypeId] int NOT NULL,
    CONSTRAINT [PK_Enterprises] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Enterprises_EnterpriseTypes_EnterpriseTypeId] FOREIGN KEY ([EnterpriseTypeId]) REFERENCES [dbo].[EnterpriseTypes] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [dbo].[Investors] (
    [Id] bigint NOT NULL IDENTITY,
    [Active] bit NOT NULL,
    [Name] varchar(50) NOT NULL,
    [Email] varchar(100) NOT NULL,
    [City] varchar(50) NOT NULL,
    [Country] varchar(70) NOT NULL,
    [Photo] nvarchar(max) NULL,
    [Balance] bigint NOT NULL DEFAULT CAST(0 AS bigint),
    [FirstAccess] bit NOT NULL DEFAULT 1,
    [SuperAngel] bit NOT NULL DEFAULT 0,
    [PortfolioValue] bigint NOT NULL DEFAULT CAST(0 AS bigint),
    [EnterpriseId] bigint NULL,
    CONSTRAINT [PK_Investors] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Investors_Enterprises_EnterpriseId] FOREIGN KEY ([EnterpriseId]) REFERENCES [dbo].[Enterprises] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [dbo].[InvestorEnterprise] (
    [InvestorId] bigint NOT NULL,
    [EnterpriseId] bigint NOT NULL,
    [Active] bit NOT NULL,
    CONSTRAINT [PK_InvestorEnterprise] PRIMARY KEY ([InvestorId], [EnterpriseId]),
    CONSTRAINT [FK_InvestorEnterprise_Enterprises_EnterpriseId] FOREIGN KEY ([EnterpriseId]) REFERENCES [dbo].[Enterprises] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_InvestorEnterprise_Investors_InvestorId] FOREIGN KEY ([InvestorId]) REFERENCES [dbo].[Investors] ([Id]) ON DELETE NO ACTION
);

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Active', N'Name') AND [object_id] = OBJECT_ID(N'[dbo].[EnterpriseTypes]'))
    SET IDENTITY_INSERT [dbo].[EnterpriseTypes] ON;
INSERT INTO [dbo].[EnterpriseTypes] ([Id], [Active], [Name])
VALUES (1, 1, 'Accounting / AI / Fintech'),
(34, 1, 'Logistics  '),
(35, 1, 'Logistics aviation'),
(36, 1, 'Marketing services'),
(38, 1, 'Media'),
(39, 1, 'Medical 3d printing'),
(41, 1, 'Nanotechnology'),
(42, 1, 'Operational Technology'),
(33, 1, 'Life sciences'),
(43, 1, 'Real estate'),
(48, 1, 'Services'),
(52, 1, 'Tech'),
(53, 1, 'Technology'),
(54, 1, 'Technology / Telecommunications / Marketing'),
(55, 1, 'Technology TI'),
(56, 1, 'Telecommunications  '),
(57, 1, 'Telecommunications - utilities'),
(44, 1, 'Road Freight Marketplace'),
(58, 1, 'TIC'),
(31, 1, 'IT/Mobility'),
(29, 1, 'IT and Software'),
(3, 1, 'Agtech'),
(4, 1, 'AI / IOT'),
(5, 1, 'App-Recycling'),
(6, 1, 'Apps'),
(7, 1, 'Artificial Intelligence'),
(8, 1, 'Big Data / Open Date'),
(10, 1, 'Cloud/SAAS'),
(30, 1, 'IT Industry / Energy'),
(12, 1, 'Construction Management Software'),
(14, 1, 'E-commerce, Marketplace B2B'),
(17, 1, 'Fintech'),
(21, 1, 'Health'),
(22, 1, 'Healthcare'),
(26, 1, 'IOT'),
(27, 1, 'IOT Cyber Security '),
(28, 1, 'IT'),
(13, 1, 'E-commerce'),
(59, 1, 'Tourism');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Active', N'Name') AND [object_id] = OBJECT_ID(N'[dbo].[EnterpriseTypes]'))
    SET IDENTITY_INSERT [dbo].[EnterpriseTypes] OFF;

GO

CREATE INDEX [IX_Enterprises_EnterpriseTypeId] ON [dbo].[Enterprises] ([EnterpriseTypeId]);

GO

CREATE INDEX [IX_InvestorEnterprise_EnterpriseId] ON [dbo].[InvestorEnterprise] ([EnterpriseId]);

GO

CREATE UNIQUE INDEX [IX_Investors_EnterpriseId] ON [dbo].[Investors] ([EnterpriseId]) WHERE [EnterpriseId] IS NOT NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20191222233241_FinalMigration', N'2.2.6-servicing-10079');

GO

