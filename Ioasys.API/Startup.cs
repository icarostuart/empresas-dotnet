﻿using AutoMapper;
using Ioasys.Data.Context;
using Ioasys.Data.Repositories;
using Ioasys.Data.Unities;
using Ioasys.Domain.Entities;
using Ioasys.Domain.Repositories;
using Ioasys.Domain.Unities;
using Ioasys.Services.ApplicationServices;
using Ioasys.Services.MapperFactories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;

namespace Ioasys.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services
                .AddHttpContextAccessor()
                // data access layer services
                .AddDbContext<EnterpriseContext>(opts => opts.UseSqlServer(Configuration.GetConnectionString("EnterpriseContext")))
                .AddScoped<IRepository<Enterprise>, Repository<Enterprise>>()
                .AddScoped<IEnterpriseRepository, EnterpriseRepository>()
                .AddScoped<IRepository<EnterpriseType>, Repository<EnterpriseType>>()
                .AddScoped<IEnterpriseTypeRepository, EnterpriseTypeRepository>()
                .AddScoped<IRepository<Investor>, Repository<Investor>>()
                .AddScoped<IInvestorRepository, InvestorRepository>()
                .AddScoped<IUnitOfEnterprises, UnitOfEnterprises>()
                // application services
                .AddScoped<IEnterpriseFacade, EnterpriseService>()
                .AddScoped<IInvestorFacade, InvestorService>()
                // net core services
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Include;
                });

            IServiceProvider provider = services.BuildServiceProvider();
            services
                // automapper services and profiles
                .AddScoped(x => new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile(new InputToDomainProfile(provider.GetRequiredService<IUnitOfEnterprises>()));
                    cfg.AddProfile(new DomainToOutputProfile());
                }).CreateMapper())
                .AddAutoMapper(typeof(EnterpriseService));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app
                .UseMiddleware<ExceptionMiddleware>()
                .UseHttpsRedirection()
                .UseMvc();
        }
    }
}
