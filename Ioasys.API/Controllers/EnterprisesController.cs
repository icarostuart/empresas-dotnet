﻿using Ioasys.API.FilterAttributes;
using Ioasys.Services.ApplicationServices;
using Ioasys.Services.DTOs.Enterprises.Inputs;
using Microsoft.AspNetCore.Mvc;

namespace Ioasys.API.Controllers
{
    [ApiController, Route("api/v1/[controller]"), Produces("application/json")]
    [ValidateModelState]
    public class EnterprisesController : ControllerBase
    {
        private readonly IEnterpriseFacade _service;

        public EnterprisesController(IEnterpriseFacade service) => _service = service;

        [HttpGet]
        public IActionResult Get([FromQuery]EnterpriseIndexFilterInput input) => Ok(_service.ListEnterprises(input));

        [HttpGet("{id}")]
        public IActionResult Get(long id) => Ok(_service.GetEnterprise(id));

        [HttpPost, UnitOfWork]
        public IActionResult Post([FromBody] CreateEnterpriseInput input)
        {
            var enterprise = _service.CreateEnterprise(input);

            return CreatedAtAction($"{nameof(Get)}", new { id = enterprise.Enterprise.Id }, enterprise);
        }

        [HttpPut("{id}"), UnitOfWork]
        public IActionResult Put(long id, [FromBody] UpdateEnterpriseInput input) => Ok(_service.UpdateEnterprise(id, input));

        [HttpDelete("{id}"), UnitOfWork]
        public IActionResult Delete(long id)
        {
            _service.DeleteEnterprise(id);

            return NoContent();
        }
    }
}
