﻿using Ioasys.API.FilterAttributes;
using Ioasys.Services.ApplicationServices;
using Ioasys.Services.DTOs.Investors.Inputs;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace Ioasys.API.Controllers
{
    [ApiController, Route("api/v1/[controller]"), Produces("application/json")]
    [ValidateModelState]
    public class InvestorsController : ControllerBase
    {
        private readonly IInvestorFacade _service;

        public InvestorsController(IInvestorFacade service) => _service = service;

        [HttpGet]
        public IActionResult Get([FromQuery]InvestorIndexFilterInput input) => Ok(_service.ListInvestors(input));

        [HttpGet("{id}")]
        public IActionResult Get(long id) => Ok(_service.GetInvestor(id));

        [HttpPost, UnitOfWork]
        public IActionResult Post([FromBody] CreateInvestorInput input)
        {
            var investor = _service.CreateInvestor(input);

            return CreatedAtAction($"{nameof(Get)}", new { id = investor.Investor.Id }, investor);
        }

        [HttpPut("{id}"), UnitOfWork]
        public IActionResult Put(long id, [FromBody] OverwriteInvestorInput input) => Ok(_service.OverwriteInvestor(id, input));

        [HttpPatch("{id}"), UnitOfWork]
        public IActionResult Patch(long id, [FromBody] JsonPatchDocument<UpdateInvestorInput> input) => Ok(_service.UpdateInvestor(id, input));

        [HttpDelete("{id}"), UnitOfWork]
        public IActionResult Delete(long id)
        {
            _service.DeleteInvestor(id);

            return NoContent();
        }
    }
}
