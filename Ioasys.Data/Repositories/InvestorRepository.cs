﻿using Ioasys.Domain.Entities;
using Ioasys.Domain.Repositories;
using Ioasys.Domain.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Ioasys.Data.Repositories
{
    public class InvestorRepository : IInvestorRepository
    {
        private readonly IRepository<Investor> _baseRepository;

        public InvestorRepository(IRepository<Investor> baseRepo)
        {
            _baseRepository = baseRepo;
        }


        public Investor LoadNavigation(Investor investor, Expression<Func<Investor, object>> expression)
        {
            return _baseRepository.LoadNavigation(investor, expression);
        }

        public Investor LoadCollection(Investor investor, string collectionPropName)
        {
            return _baseRepository.LoadCollection(investor, collectionPropName);
        }

        public IQueryable<Investor> GetAll(bool readOnly = false)
        {
            return _baseRepository.GetAll(readOnly);
        }

        public IQueryable<Investor> GetAll(string included = "", bool readOnly = false)
        {
            return _baseRepository.GetAll(included, readOnly);
        }

        public Investor GetWithKeys(long id)
        {
            return _baseRepository.GetWithKeys(id);
        }

        public Investor GetWithKeys(long id, IEnumerable<string> navigations = null, IEnumerable<string> collections = null)
        {
            return _baseRepository.GetWithKeys(new[] { (object)id }, navigations: navigations, collections: collections);
        }

        public Investor Insert(Investor enterprise)
        {
            return _baseRepository.Insert(enterprise);
        }

        public IEnumerable<Investor> InsertMany(IEnumerable<Investor> enterprises)
        {
            return _baseRepository.InsertMany(enterprises);
        }

        public Pagination Paginate(Pagination pagination)
        {
            return _baseRepository.Paginate(pagination);
        }

        public IQueryable<Investor> Query(Expression<Func<Investor, bool>> predicate = null, bool readOnly = false, string included = "")
        {
            return _baseRepository.Query(predicate, readOnly, included);
        }
    }
}
