﻿using Ioasys.Domain.Entities;
using Ioasys.Domain.Repositories;
using Ioasys.Domain.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Ioasys.Data.Repositories
{
    public class EnterpriseRepository : IEnterpriseRepository
    {
        private readonly IRepository<Enterprise> _baseRepository;

        public EnterpriseRepository(IRepository<Enterprise> baseRepo)
        {
            _baseRepository = baseRepo;
        }

        public Enterprise LoadNavigation(Enterprise enterprise, Expression<Func<Enterprise, object>> expression)
        {
            return _baseRepository.LoadNavigation(enterprise, expression);
        }

        public Enterprise LoadCollection(Enterprise enterprise, string collectionPropName)
        {
            return _baseRepository.LoadCollection(enterprise, collectionPropName);
        }

        public IQueryable<Enterprise> GetAll(bool readOnly = false)
        {
            return _baseRepository.GetAll(readOnly);
        }

        public IQueryable<Enterprise> GetAll(string included = "", bool readOnly = false)
        {
            return _baseRepository.GetAll(included, readOnly);
        }

        public Enterprise GetWithKeys(long id)
        {
            return _baseRepository.GetWithKeys(id);
        }

        public Enterprise GetWithKeys(long id, IEnumerable<string> navigations = null, IEnumerable<string> collections = null)
        {
            return _baseRepository.GetWithKeys(new[] { (object)id }, navigations: navigations, collections: collections);
        }

        public Enterprise Insert(Enterprise enterprise)
        {
            return _baseRepository.Insert(enterprise);
        }

        public IEnumerable<Enterprise> InsertMany(IEnumerable<Enterprise> enterprises)
        {
            return _baseRepository.InsertMany(enterprises);
        }

        public Pagination Paginate(Pagination pagination)
        {
            return _baseRepository.Paginate(pagination);
        }

        public IQueryable<Enterprise> Query(Expression<Func<Enterprise, bool>> predicate = null, bool readOnly = false, string included = "")
        {
            return _baseRepository.Query(predicate, readOnly, included);
        }

        public Enterprise Remove(Enterprise enterprise)
        {
            return _baseRepository.Remove(enterprise);
        }

        public Enterprise Remove(long id)
        {
            return _baseRepository.Remove(id);
        }

        public IEnumerable<Enterprise> RemoveMany(IEnumerable<Enterprise> enterprises)
        {
            return _baseRepository.RemoveMany(enterprises);
        }

        public IEnumerable<Enterprise> RemoveMany(IEnumerable<long> ids)
        {
            var enterprises = GetAll(false).Join(ids, e => e.Id, id => id, (e, id) => e);

            return _baseRepository.RemoveMany(enterprises);
        }
    }
}
