﻿using Ioasys.Domain.Entities;
using Ioasys.Domain.Repositories;
using Ioasys.Domain.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Ioasys.Data.Repositories
{
    public class EnterpriseTypeRepository : IEnterpriseTypeRepository
    {
        private readonly IRepository<EnterpriseType> _baseRepository;

        public EnterpriseTypeRepository(IRepository<EnterpriseType> baseRepo)
        {
            _baseRepository = baseRepo;
        }

        public IQueryable<EnterpriseType> GetAll(bool readOnly = false)
        {
            return _baseRepository.GetAll(readOnly);
        }

        public IQueryable<EnterpriseType> GetAll(string included = "", bool readOnly = false)
        {
            return _baseRepository.GetAll(included, readOnly);
        }

        public EnterpriseType GetWithKeys(int id)
        {
            return _baseRepository.GetWithKeys(id);
        }

        public EnterpriseType GetWithKeys(int id, IEnumerable<string> navigations = null, IEnumerable<string> collections = null)
        {
            return _baseRepository.GetWithKeys(new[] { (object)id }, navigations: navigations, collections: collections);
        }

        public EnterpriseType Insert(EnterpriseType enterprise)
        {
            return _baseRepository.Insert(enterprise);
        }

        public IEnumerable<EnterpriseType> InsertMany(IEnumerable<EnterpriseType> enterprises)
        {
            return _baseRepository.InsertMany(enterprises);
        }

        public Pagination Paginate(Pagination pagination)
        {
            return _baseRepository.Paginate(pagination);
        }

        public IQueryable<EnterpriseType> Query(Expression<Func<EnterpriseType, bool>> predicate = null, bool readOnly = false, string included = "")
        {
            return _baseRepository.Query(predicate, readOnly, included);
        }

        public EnterpriseType Remove(EnterpriseType enterprise)
        {
            return _baseRepository.Remove(enterprise);
        }

        public EnterpriseType Remove(int id)
        {
            return _baseRepository.Remove(id);
        }

        public IEnumerable<EnterpriseType> RemoveMany(IEnumerable<EnterpriseType> enterprises)
        {
            return _baseRepository.RemoveMany(enterprises);
        }

        public IEnumerable<EnterpriseType> RemoveMany(IEnumerable<int> ids)
        {
            var enterprises = GetAll(false).Join(ids, e => e.Id, id => id, (e, id) => e);

            return _baseRepository.RemoveMany(enterprises);
        }
    }
}
