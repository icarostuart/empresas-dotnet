﻿using Ioasys.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Ioasys.Data.Context
{
    public class EnterpriseContext : DbContext
    {
        public DbSet<Investor> Investors { get; set; }
        public DbSet<Enterprise> Enterprises { get; set; }
        public DbSet<EnterpriseType> EnterpriseTypes { get; set; }

        public EnterpriseContext(DbContextOptions<EnterpriseContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("dbo");
            modelBuilder.Ignore<BaseEntity>();
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
