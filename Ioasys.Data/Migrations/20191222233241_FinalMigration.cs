﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Ioasys.Data.Migrations
{
    public partial class FinalMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "dbo");

            migrationBuilder.CreateTable(
                name: "EnterpriseTypes",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(type: "varchar(50)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnterpriseTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Enterprises",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(type: "varchar(50)", nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Value = table.Column<long>(nullable: false, defaultValue: 0L),
                    City = table.Column<string>(type: "varchar(50)", nullable: false),
                    Country = table.Column<string>(type: "varchar(70)", nullable: false),
                    Email = table.Column<string>(type: "varchar(100)", nullable: true),
                    Facebook = table.Column<string>(type: "varchar(200)", nullable: true),
                    Twitter = table.Column<string>(type: "varchar(100)", nullable: true),
                    Linkedin = table.Column<string>(type: "varchar(200)", nullable: true),
                    Phone = table.Column<string>(type: "varchar(14)", nullable: true),
                    Photo = table.Column<string>(nullable: true),
                    Shares = table.Column<long>(nullable: false),
                    SharePrice = table.Column<long>(nullable: false),
                    OwnShares = table.Column<long>(nullable: false),
                    OwnEnterprise = table.Column<bool>(nullable: false, defaultValue: false),
                    EnterpriseTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enterprises", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Enterprises_EnterpriseTypes_EnterpriseTypeId",
                        column: x => x.EnterpriseTypeId,
                        principalSchema: "dbo",
                        principalTable: "EnterpriseTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Investors",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(type: "varchar(50)", nullable: false),
                    Email = table.Column<string>(type: "varchar(100)", nullable: false),
                    City = table.Column<string>(type: "varchar(50)", nullable: false),
                    Country = table.Column<string>(type: "varchar(70)", nullable: false),
                    Photo = table.Column<string>(nullable: true),
                    Balance = table.Column<long>(nullable: false, defaultValue: 0L),
                    FirstAccess = table.Column<bool>(nullable: false, defaultValue: true),
                    SuperAngel = table.Column<bool>(nullable: false, defaultValue: false),
                    PortfolioValue = table.Column<long>(nullable: false, defaultValue: 0L),
                    EnterpriseId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Investors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Investors_Enterprises_EnterpriseId",
                        column: x => x.EnterpriseId,
                        principalSchema: "dbo",
                        principalTable: "Enterprises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InvestorEnterprise",
                schema: "dbo",
                columns: table => new
                {
                    InvestorId = table.Column<long>(nullable: false),
                    EnterpriseId = table.Column<long>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvestorEnterprise", x => new { x.InvestorId, x.EnterpriseId });
                    table.ForeignKey(
                        name: "FK_InvestorEnterprise_Enterprises_EnterpriseId",
                        column: x => x.EnterpriseId,
                        principalSchema: "dbo",
                        principalTable: "Enterprises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InvestorEnterprise_Investors_InvestorId",
                        column: x => x.InvestorId,
                        principalSchema: "dbo",
                        principalTable: "Investors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                schema: "dbo",
                table: "EnterpriseTypes",
                columns: new[] { "Id", "Active", "Name" },
                values: new object[,]
                {
                    { 1, true, "Accounting / AI / Fintech" },
                    { 34, true, "Logistics  " },
                    { 35, true, "Logistics aviation" },
                    { 36, true, "Marketing services" },
                    { 38, true, "Media" },
                    { 39, true, "Medical 3d printing" },
                    { 41, true, "Nanotechnology" },
                    { 42, true, "Operational Technology" },
                    { 33, true, "Life sciences" },
                    { 43, true, "Real estate" },
                    { 48, true, "Services" },
                    { 52, true, "Tech" },
                    { 53, true, "Technology" },
                    { 54, true, "Technology / Telecommunications / Marketing" },
                    { 55, true, "Technology TI" },
                    { 56, true, "Telecommunications  " },
                    { 57, true, "Telecommunications - utilities" },
                    { 44, true, "Road Freight Marketplace" },
                    { 58, true, "TIC" },
                    { 31, true, "IT/Mobility" },
                    { 29, true, "IT and Software" },
                    { 3, true, "Agtech" },
                    { 4, true, "AI / IOT" },
                    { 5, true, "App-Recycling" },
                    { 6, true, "Apps" },
                    { 7, true, "Artificial Intelligence" },
                    { 8, true, "Big Data / Open Date" },
                    { 10, true, "Cloud/SAAS" },
                    { 30, true, "IT Industry / Energy" },
                    { 12, true, "Construction Management Software" },
                    { 14, true, "E-commerce, Marketplace B2B" },
                    { 17, true, "Fintech" },
                    { 21, true, "Health" },
                    { 22, true, "Healthcare" },
                    { 26, true, "IOT" },
                    { 27, true, "IOT Cyber Security " },
                    { 28, true, "IT" },
                    { 13, true, "E-commerce" },
                    { 59, true, "Tourism" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Enterprises_EnterpriseTypeId",
                schema: "dbo",
                table: "Enterprises",
                column: "EnterpriseTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_InvestorEnterprise_EnterpriseId",
                schema: "dbo",
                table: "InvestorEnterprise",
                column: "EnterpriseId");

            migrationBuilder.CreateIndex(
                name: "IX_Investors_EnterpriseId",
                schema: "dbo",
                table: "Investors",
                column: "EnterpriseId",
                unique: true,
                filter: "[EnterpriseId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InvestorEnterprise",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Investors",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Enterprises",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "EnterpriseTypes",
                schema: "dbo");
        }
    }
}
