﻿using Ioasys.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ioasys.Data.Maps
{
    public class InvestorMap : IEntityTypeConfiguration<Investor>
    {
        public void Configure(EntityTypeBuilder<Investor> builder)
        {
            builder.Property(x => x.Id).IsRequired();
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasColumnType("varchar(50)").IsRequired();
            builder.Property(x => x.Balance).IsRequired().HasDefaultValue(0);
            builder.Property(x => x.PortfolioValue).IsRequired().HasDefaultValue(0);
            builder.Property(x => x.FirstAccess).IsRequired().HasDefaultValue(true);
            builder.Property(x => x.SuperAngel).IsRequired().HasDefaultValue(false);

            builder.HasOne<Enterprise>(u => u.Enterprise)
                   .WithOne(c => c.Owner)
                   .HasForeignKey<Investor>(c => c.EnterpriseId)
                   .IsRequired(false)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.Ignore(x => x.Enterprises);

            builder.OwnsOne(x => x.Email, email =>
            {
                email.Property(x => x.Address).HasColumnType("varchar(100)").HasColumnName("Email").IsRequired();
            });

            builder.OwnsOne(x => x.Location, local =>
            {
                local.Property(x => x.City).HasColumnType("varchar(50)").HasColumnName("City").IsRequired();
                local.Property(x => x.Country).HasColumnType("varchar(70)").HasColumnName("Country").IsRequired();
            });
        }
    }
}
