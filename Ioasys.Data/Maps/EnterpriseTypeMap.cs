﻿using Ioasys.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System.IO;

namespace Ioasys.Data.Maps
{
    public class EnterpriseTypeMap : IEntityTypeConfiguration<EnterpriseType>
    {
        public void Configure(EntityTypeBuilder<EnterpriseType> builder)
        {
            builder.Property(x => x.Id).IsRequired();
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasColumnType("varchar(50)").IsRequired();

            using StreamReader stream = new StreamReader("..\\Ioasys.Data\\JsonData\\EnterpriseTypes.json");
            var types = JsonConvert.DeserializeObject<EnterpriseType[]>(stream.ReadToEnd());

            builder.HasData(types);
        }
    }
}
