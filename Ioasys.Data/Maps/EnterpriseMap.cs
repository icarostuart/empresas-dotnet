﻿using Ioasys.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ioasys.Data.Maps
{
    public class EnterpriseMap : IEntityTypeConfiguration<Enterprise>
    {
        public void Configure(EntityTypeBuilder<Enterprise> builder)
        {
            builder.Property(x => x.Id).IsRequired();
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasColumnType("varchar(50)").IsRequired();
            builder.Property(x => x.Description).IsRequired();
            builder.Property(x => x.Value).IsRequired().HasDefaultValue(0);
            builder.Property(x => x.OwnEnterprise).IsRequired().HasDefaultValue(0);

            builder.HasOne<EnterpriseType>(u => u.EnterpriseType)
                   .WithMany(c => c.Enterprises)
                   .HasForeignKey(c => c.EnterpriseTypeId)
                   .IsRequired()
                   .OnDelete(DeleteBehavior.Restrict);

            builder.Ignore(x => x.Investors);

            builder.OwnsOne(x => x.Email, email =>
            {
                email.Property(x => x.Address).HasColumnType("varchar(100)").HasColumnName("Email").IsRequired(false);
            });

            builder.OwnsOne(x => x.Contact, contact =>
            {
                contact.Property(x => x.Facebook).HasColumnType("varchar(200)").HasColumnName("Facebook").IsRequired(false);
                contact.Property(x => x.Twitter).HasColumnType("varchar(100)").HasColumnName("Twitter").IsRequired(false);
                contact.Property(x => x.Linkedin).HasColumnType("varchar(200)").HasColumnName("Linkedin").IsRequired(false);
                contact.Property(x => x.Phone).HasColumnType("varchar(14)").HasColumnName("Phone").IsRequired(false);
            });

            builder.OwnsOne(x => x.Location, local =>
            {
                local.Property(x => x.City).HasColumnType("varchar(50)").HasColumnName("City").IsRequired();
                local.Property(x => x.Country).HasColumnType("varchar(70)").HasColumnName("Country").IsRequired();
            });
        }
    }
}
