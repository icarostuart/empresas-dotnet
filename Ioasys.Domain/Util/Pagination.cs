﻿using Ioasys.Domain.Enums;
using System.Collections.Generic;

namespace Ioasys.Domain.Util
{
    public class Pagination
    {
        public int Page { get; set; } = 1;
        public int PageLength { get; set; } = 10;
        public string Term { get; set; } = "";
        public Orderby Order { get; set; } = Orderby.ascending; //"ascending";
        public string Column { get; set; } = "Id";
        public decimal TotalInPage { get; set; } = 0m;
        public decimal Total { get; set; } = 0m;
        public IReadOnlyList<dynamic> Items { get; set; } = new List<object>();

        public long Length { get; set; }
        public long Start => Items.Count == 0 ? 0 : (PageLength * (Page - 1)) + 1;
        public long End => Items.Count == 0 ? 0 : Start + (Items.Count - 1);
        public long Pages => Length == 0 ? 1 : (Length + PageLength - 1) / PageLength;
        public string TotalInPageAsCurrency => TotalInPage.AsBRL();
        public string TotalEmMoeda => Total.AsBRL();
    }
}
