﻿using System.Globalization;

namespace Ioasys.Domain.Util
{
    public static class CurrencyExtension
    {
        public static string AsSpecificCurrency(this decimal value, CultureInfo culture) =>value.ToString("C", culture);

        public static string AsBRL(this decimal value) => AsSpecificCurrency(value, new CultureInfo("pt-BR"));

        public static string AsUnsignedSpecificCurrency(this decimal value, CultureInfo culture) => value.ToString(culture);

        public static string AsUnsignedBRL(this decimal value) => value.ToString(new CultureInfo("pt-BR"));
    }
}
