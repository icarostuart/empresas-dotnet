﻿using Ioasys.Domain.ComplexTypes;
using System.Collections.Generic;
using System.Linq;

namespace Ioasys.Domain.Entities
{
    public class Investor : BaseEntity
    {
        protected Investor() { } // entityframework query constructor

        public Investor(string name, Email email, Location location)
        {
            Name = name;
            Email = email;
            Location = location;
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public Email Email { get; set; }
        public Location Location { get; set; }
        public string Photo { get; set; }
        public long Balance { get; set; }
        public bool? FirstAccess { get; protected set; } = true;
        public bool SuperAngel { get; set; }
        public long PortfolioValue { get; set; }

        public long? EnterpriseId { get; set; }
        public virtual Enterprise Enterprise { get; set; }

        public virtual ICollection<InvestorEnterprise> InvestorsEnterprises { get; set; } = new List<InvestorEnterprise>();
        public virtual List<Enterprise> Enterprises => InvestorsEnterprises.Select(ie => ie.Enterprise).ToList();


        public Investor UpdateLocation(Location location)
        {
            Location = location;

            return this;
        }

        public Investor UpdateEmail(Email email)
        {
            Email = email;

            return this;
        }

        public Investor UpdatePortfolioValue()
        {
            PortfolioValue = Enterprises.Sum(e => e.Value);

            return this;
        }

        public Investor UpdateAccessAfterLogin()
        {
            FirstAccess = false;

            return this;
        }
    }
}
