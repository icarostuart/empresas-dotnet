﻿using System;
using System.Linq.Expressions;

namespace Ioasys.Domain.Entities
{
    public abstract class BaseEntity
    {
        public virtual bool Active { get; set; } = true;
    }
}
