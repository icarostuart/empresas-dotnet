﻿using Ioasys.Domain.ComplexTypes;
using System.Collections.Generic;
using System.Linq;

namespace Ioasys.Domain.Entities
{
    public class Enterprise : BaseEntity
    {
        protected Enterprise() { } // entityframework query constructor

        public Enterprise(string name, string description, EnterpriseType type, Location location)
        {
            Name = name;
            Description = description;
            EnterpriseType = type;
            Location = location;
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long Value { get; set; }
        public Location Location { get; set; }
        public Email Email { get; set; } = new Email(null);
        public Contact Contact { get; set; } = new Contact();
        public string Photo { get; set; }
        public long Shares { get; set; }
        public long SharePrice { get; set; }
        public long OwnShares { get; set; }
        public bool OwnEnterprise { get; set; }

        public int EnterpriseTypeId { get; set; }
        public virtual EnterpriseType EnterpriseType { get; set; } = null;
        public virtual Investor Owner { get; set; } = null;

        public virtual ICollection<InvestorEnterprise> InvestorsEnterprises { get; set; } = new List<InvestorEnterprise>();
        public virtual List<Investor> Investors => InvestorsEnterprises?.Select(ie => ie.Investor).ToList() ?? new List<Investor>();

        public Enterprise UpdateEmail(Email email)
        {
            Email = email;

            return this;
        }
    }
}
