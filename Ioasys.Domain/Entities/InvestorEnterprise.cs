﻿namespace Ioasys.Domain.Entities
{
    public class InvestorEnterprise : BaseEntity
    {
        public long InvestorId { get; set; }
        public long EnterpriseId { get; set; }

        public virtual Investor Investor { get; set; }
        public virtual Enterprise Enterprise { get; set; }
    }
}
