﻿namespace Ioasys.Domain.ComplexTypes
{
    public class Contact
    {
        public Contact() { }

        public Contact(string phone, string linkedid, string facebook, string twitter)
        {
            Phone = phone;
            Linkedin = linkedid;
            Facebook = facebook;
            Twitter = twitter;
        }

        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Linkedin { get; set; }
        public string Phone { get; set; }
    }
}
