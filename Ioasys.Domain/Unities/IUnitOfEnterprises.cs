﻿using Ioasys.Domain.Repositories;

namespace Ioasys.Domain.Unities
{
    public interface IUnitOfEnterprises : IUnitOfWork
    {
        public IEnterpriseRepository Enterprises { get; }
        public IInvestorRepository Investors { get; }
        public IEnterpriseTypeRepository Types { get; }
    }
}
