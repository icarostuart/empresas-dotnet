﻿using Ioasys.Domain.Entities;
using Ioasys.Domain.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Ioasys.Domain.Repositories
{
    public interface IInvestorRepository
    {
        Investor LoadNavigation(Investor investor, Expression<Func<Investor, object>> expression);

        Investor LoadCollection(Investor investor, string collectionPropName);

        Investor GetWithKeys(long id);

        Investor GetWithKeys(long id, IEnumerable<string> navigations = null, IEnumerable<string> collections = null);

        IQueryable<Investor> GetAll(bool readOnly = false);

        IQueryable<Investor> GetAll(string included = "", bool readOnly = false);

        IQueryable<Investor> Query(Expression<Func<Investor, bool>> predicate = null, bool readOnly = false, string included = "");

        Pagination Paginate(Pagination pagination);

        Investor Insert(Investor entity);

        IEnumerable<Investor> InsertMany(IEnumerable<Investor> entities);
    }
}
