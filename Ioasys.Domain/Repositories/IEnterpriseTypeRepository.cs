﻿using Ioasys.Domain.Entities;
using Ioasys.Domain.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Ioasys.Domain.Repositories
{
    public interface IEnterpriseTypeRepository
    {
        EnterpriseType GetWithKeys(int id);

        EnterpriseType GetWithKeys(int id, IEnumerable<string> navigations = null, IEnumerable<string> collections = null);

        IQueryable<EnterpriseType> GetAll(bool readOnly = false);

        IQueryable<EnterpriseType> GetAll(string included = "", bool readOnly = false);

        IQueryable<EnterpriseType> Query(Expression<Func<EnterpriseType, bool>> predicate = null, bool readOnly = false, string included = "");

        Pagination Paginate(Pagination pagination);

        EnterpriseType Insert(EnterpriseType entity);

        IEnumerable<EnterpriseType> InsertMany(IEnumerable<EnterpriseType> entities);

        EnterpriseType Remove(EnterpriseType entity);

        EnterpriseType Remove(int id);

        IEnumerable<EnterpriseType> RemoveMany(IEnumerable<EnterpriseType> entities);

        IEnumerable<EnterpriseType> RemoveMany(IEnumerable<int> ids);
    }
}
