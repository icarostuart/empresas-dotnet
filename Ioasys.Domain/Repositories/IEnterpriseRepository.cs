﻿using Ioasys.Domain.Entities;
using Ioasys.Domain.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Ioasys.Domain.Repositories
{
    public interface IEnterpriseRepository
    {
        Enterprise LoadNavigation(Enterprise enterprise, Expression<Func<Enterprise, object>> expression);

        Enterprise LoadCollection(Enterprise enterprise, string collectionPropName);

        Enterprise GetWithKeys(long id);

        Enterprise GetWithKeys(long id, IEnumerable<string> navigations = null, IEnumerable<string> collections = null);

        IQueryable<Enterprise> GetAll(bool readOnly = false);

        IQueryable<Enterprise> GetAll(string included = "", bool readOnly = false);

        IQueryable<Enterprise> Query(Expression<Func<Enterprise, bool>> predicate = null, bool readOnly = false, string included = "");

        Pagination Paginate(Pagination pagination);

        Enterprise Insert(Enterprise entity);

        IEnumerable<Enterprise> InsertMany(IEnumerable<Enterprise> entities);

        Enterprise Remove(Enterprise entity);

        Enterprise Remove(long id);

        IEnumerable<Enterprise> RemoveMany(IEnumerable<Enterprise> entities);

        IEnumerable<Enterprise> RemoveMany(IEnumerable<long> ids);
    }
}
