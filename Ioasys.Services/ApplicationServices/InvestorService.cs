﻿using AutoMapper;
using Ioasys.Domain.Entities;
using Ioasys.Domain.Repositories;
using Ioasys.Domain.Unities;
using Ioasys.Domain.Util;
using Ioasys.Services.DTOs.Investors.Inputs;
using Ioasys.Services.DTOs.Investors.Outputs;
using Microsoft.AspNetCore.JsonPatch;
using System.Linq;

namespace Ioasys.Services.ApplicationServices
{
    public class InvestorService : IInvestorFacade
    {
        private readonly IUnitOfEnterprises _uow;
        private readonly IInvestorRepository _investors;
        private readonly IMapper _mapper;

        public InvestorService(IUnitOfEnterprises uow, IMapper mapper)
        {
            _uow = uow;
            _investors = uow.Investors;
            _mapper = mapper;
        }

        // POST
        public InvestorDetailGroupOutput CreateInvestor(CreateInvestorInput input)
        {
            var investor = _investors.Insert(_mapper.Map<Investor>(input));

            _uow.Save();

            return _mapper.Map<InvestorDetailGroupOutput>(investor);
        }

        // GET{id}
        public InvestorDetailGroupOutput GetInvestor(long id)
        {
            var investor = _mapper.Map<Investor>(id);

            return investor.Active
                ? _mapper.Map<InvestorDetailGroupOutput>(investor)
                : throw new ApiException(System.Net.HttpStatusCode.Gone, "Gone");
        }

        // GET?filters
        public InvestorIndexGroupOutput ListInvestors(InvestorIndexFilterInput input)
        {
            var filterObject = _mapper.Map<Investor>(input);
            var query = _investors.Query(
                e => e.Active,
                readOnly: true,
                included: $"{nameof(Investor.Enterprise)}" +
                $",{nameof(Investor.InvestorsEnterprises)}" +
                $",{nameof(Investor.InvestorsEnterprises)}.{nameof(InvestorEnterprise.Enterprise)}" +
                $",{nameof(Investor.InvestorsEnterprises)}.{nameof(InvestorEnterprise.Enterprise)}.{nameof(Enterprise.EnterpriseType)}");

            // poorly made. not discovered yet a simpler and better way to enable all properties as filters but checking each one
            query = query.Where(e => filterObject.Name == e.Name || input.Name == null);
            query = query.Where(e => filterObject.Id == e.Id || input.Id == null);
            query = query.Where(e => filterObject.PortfolioValue == e.PortfolioValue || input.PortfolioValue == null);
            query = query.Where(e => filterObject.Balance == e.Balance || input.Balance == null);
            query = query.Where(e => filterObject.EnterpriseId == e.EnterpriseId || input.EnterpriseId == null);
            query = query.Where(e => filterObject.Email.Address == e.Email.Address || input.Email == null);
            query = query.Where(e => filterObject.Location.City == e.Location.City || input.City == null);
            query = query.Where(e => filterObject.Location.Country == e.Location.Country || input.Country == null);
            query = query.Where(e => filterObject.SuperAngel == e.SuperAngel || input.SuperAngel == null);
            query = query.Where(e => filterObject.FirstAccess == e.FirstAccess || input.FirstAccess == null);

            return _mapper.Map<InvestorIndexGroupOutput>(query.ToList());
        }

        // PUT{id}
        public InvestorDetailGroupOutput OverwriteInvestor(long id, OverwriteInvestorInput input)
        {
            if (id == input.Id)
            {
                var investor = _mapper.Map<Investor>(input);

                return _mapper.Map<InvestorDetailGroupOutput>(investor);
            }
            throw new ApiException(System.Net.HttpStatusCode.Conflict, $"conflict between id [{id}] and input id [{input.Id}]");
        }

        // PATCH{id}
        public InvestorDetailGroupOutput UpdateInvestor(long id, JsonPatchDocument<UpdateInvestorInput> input)
        {
            var investor = _mapper.Map<Investor>(id);

            var delta = _mapper.Map<JsonPatchDocument<UpdateInvestorInput>, JsonPatchDocument<Investor>>(input);
            delta.ApplyTo(investor);

            return _mapper.Map<InvestorDetailGroupOutput>(investor);
        }

        // DELETE{id}
        public void DeleteInvestor(long id)
        {
            var investor = _mapper.Map<Investor>(id);

            investor.Active = false;
        }
    }
}
