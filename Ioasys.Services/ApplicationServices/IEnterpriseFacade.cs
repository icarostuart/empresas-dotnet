﻿using Ioasys.Services.DTOs.Enterprises.Inputs;
using Ioasys.Services.DTOs.Enterprises.Outputs;

namespace Ioasys.Services.ApplicationServices
{
    public interface IEnterpriseFacade
    {
        EnterpriseDetailGroupOutput CreateEnterprise(CreateEnterpriseInput input); // POST
        EnterpriseDetailGroupOutput GetEnterprise(long id); // GET{id}
        EnterpriseIndexGroupOutput ListEnterprises(EnterpriseIndexFilterInput payload); // GET?filters
        EnterpriseDetailGroupOutput UpdateEnterprise(long id, UpdateEnterpriseInput input); // PUT{id}
        void DeleteEnterprise(long id); // DELETE{id}
    }
}
