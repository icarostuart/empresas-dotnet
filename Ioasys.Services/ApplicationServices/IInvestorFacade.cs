﻿using Ioasys.Services.DTOs.Investors.Inputs;
using Ioasys.Services.DTOs.Investors.Outputs;
using Microsoft.AspNetCore.JsonPatch;

namespace Ioasys.Services.ApplicationServices
{
    public interface IInvestorFacade
    {
        InvestorDetailGroupOutput CreateInvestor(CreateInvestorInput input); // POST
        InvestorDetailGroupOutput GetInvestor(long id); // GET{id}
        InvestorIndexGroupOutput ListInvestors(InvestorIndexFilterInput payload); // GET?filters
        InvestorDetailGroupOutput OverwriteInvestor(long id, OverwriteInvestorInput input); // PUT{id}
        InvestorDetailGroupOutput UpdateInvestor(long id, JsonPatchDocument<UpdateInvestorInput> input); // PATCH{id}
        void DeleteInvestor(long id); // DELETE{id}
    }
}
