﻿using AutoMapper;
using Ioasys.Domain.Entities;
using Ioasys.Domain.Repositories;
using Ioasys.Domain.Unities;
using Ioasys.Domain.Util;
using Ioasys.Services.DTOs.Enterprises.Inputs;
using Ioasys.Services.DTOs.Enterprises.Outputs;
using System.Linq;
using System.Net;

namespace Ioasys.Services.ApplicationServices
{
    public class EnterpriseService : IEnterpriseFacade
    {
        private readonly IUnitOfEnterprises _uow;
        private readonly IEnterpriseRepository _enterprises;
        private readonly IEnterpriseTypeRepository _types;
        private readonly IMapper _mapper;

        public EnterpriseService(IUnitOfEnterprises uow, IMapper mapper)
        {
            _uow = uow;
            _enterprises = uow.Enterprises;
            _types = uow.Types;
            _mapper = mapper;
        }

        // POST
        public EnterpriseDetailGroupOutput CreateEnterprise(CreateEnterpriseInput input)
        {
            var enterprise = _mapper.Map<Enterprise>(input);
            enterprise.EnterpriseType = _types.GetWithKeys(enterprise.EnterpriseTypeId) 
                ?? throw new ApiException(HttpStatusCode.Conflict, $"Invalid Id {input.EnterpriseTypeId} for EnterpriseType");

            enterprise = _enterprises.Insert(enterprise);
            _uow.Save();

            return _mapper.Map<EnterpriseDetailGroupOutput>(enterprise);
        }

        // GET{id}
        public EnterpriseDetailGroupOutput GetEnterprise(long id)
        {
            var enterprise = _mapper.Map<Enterprise>(id);

            return _mapper.Map<EnterpriseDetailGroupOutput>(enterprise);
        }

        // GET?filters
        public EnterpriseIndexGroupOutput ListEnterprises(EnterpriseIndexFilterInput input)
        {
            var filterObject = _mapper.Map<Enterprise>(input);
            var query = _enterprises.Query(
                e => e.Active,
                readOnly: true,
                included: $"{nameof(Enterprise.EnterpriseType)},{nameof(Enterprise.Owner)},{nameof(Enterprise.InvestorsEnterprises)}");

            // required filters
            query = query.Where(e => filterObject.Name == e.Name || input.Name == null);
            query = query.Where(e => filterObject.EnterpriseTypeId == e.EnterpriseTypeId || input.EnterpriseTypeId == null);

            // aditional filters. poorly made. not discovered yet a simpler and better way to enable all properties as filters but checking each one
            query = query.Where(e => filterObject.Contact.Phone == e.Contact.Phone || input.Phone == null);
            query = query.Where(e => filterObject.Contact.Twitter == e.Contact.Twitter || input.Twitter == null);
            query = query.Where(e => filterObject.Contact.Facebook == e.Contact.Facebook || input.Facebook == null);
            query = query.Where(e => filterObject.Contact.Linkedin == e.Contact.Linkedin || input.Linkedin == null);
            query = query.Where(e => filterObject.Email.Address == e.Email.Address || input.Email == null);
            query = query.Where(e => filterObject.Location.City == e.Location.City || input.City == null);
            query = query.Where(e => filterObject.Location.Country == e.Location.Country || input.Country == null);
            query = query.Where(e => filterObject.Value == e.Value || input.Value == null);
            query = query.Where(e => filterObject.Id == e.Id || input.Id == null);
            query = query.Where(e => filterObject.OwnEnterprise == e.OwnEnterprise || input.OwnEnterprise == null);

            return _mapper.Map<EnterpriseIndexGroupOutput>(query.ToList());
        }

        // PUT{id}
        public EnterpriseDetailGroupOutput UpdateEnterprise(long id, UpdateEnterpriseInput input)
        {
            if (id == input.Id)
            {
                var enterprise = _mapper.Map<Enterprise>(input);
                enterprise.EnterpriseType = _types.GetWithKeys(enterprise.EnterpriseTypeId)
                    ?? throw new ApiException(HttpStatusCode.Conflict, $"Invalid Id {input.EnterpriseTypeId} for EnterpriseType");

                return _mapper.Map<EnterpriseDetailGroupOutput>(enterprise);
            }
            throw new ApiException(HttpStatusCode.Conflict, $"conflict between id [{id}] and input id [{input.Id}]");
        }

        // DELETE{id}
        public void DeleteEnterprise(long id)
        {
            var enterprise = _mapper.Map<Enterprise>(id);

            _enterprises.Remove(enterprise);
        }
    }
}
