﻿using Ioasys.Services.DTOs.Enterprises.Outputs;
using Ioasys.Services.DTOs.Investors.Outputs;
using Newtonsoft.Json;

namespace Ioasys.Services.DTOs.Auth.Outputs
{
    public partial class SuccessSigninOutput
    {
        [JsonProperty("investor")]
        public InvestorOutput Investor { get; set; }

        [JsonProperty("enterprise")]
        public EnterpriseDetailOutput Enterprise { get; set; }

        [JsonProperty("success")]
        public bool Success => true;
    }
}
