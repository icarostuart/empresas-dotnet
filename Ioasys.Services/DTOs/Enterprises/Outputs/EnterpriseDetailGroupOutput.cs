﻿using Newtonsoft.Json;

namespace Ioasys.Services.DTOs.Enterprises.Outputs
{
    public partial class EnterpriseDetailGroupOutput
    {
        [JsonProperty("enterprise")] public EnterpriseDetailOutput Enterprise { get; set; }
        [JsonProperty("success")] public bool Success => true;
    }
}
