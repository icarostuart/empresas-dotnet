﻿using Ioasys.Domain.Entities;
using Newtonsoft.Json;

namespace Ioasys.Services.DTOs.Enterprises.Outputs
{
    public partial class EnterpriseIndexOutput
    {
        public EnterpriseIndexOutput()
        {

        }

        public EnterpriseIndexOutput(Enterprise enterprise)
        {
            Id = enterprise.Id;
            Name = enterprise.Name;
            Description = enterprise.Description;
            Email = enterprise.Email.Address;
            Photo = enterprise.Photo;
            Facebook = enterprise.Contact.Facebook;
            Twitter = enterprise.Contact.Twitter;
            Linkedin = enterprise.Contact.Linkedin;
            Phone = enterprise.Contact.Phone;
            City = enterprise.Location.City;
            Country = enterprise.Location.Country;
            OwnEnterprise = enterprise.OwnEnterprise;
            Value = enterprise.Value;
            SharePrice = enterprise.SharePrice;
            EnterpriseType = new EnterpriseTypeOutput
            {
                Id = enterprise.EnterpriseType.Id,
                Name = enterprise.EnterpriseType.Name
            };
        }

        [JsonProperty("id")] public long Id { get; set; }
        [JsonProperty("enterprise_name")] public string Name { get; set; }
        [JsonProperty("description")] public string Description { get; set; }
        [JsonProperty("city")] public string City { get; set; }
        [JsonProperty("country")] public string Country { get; set; }
        [JsonProperty("value")] public long Value { get; set; }
        [JsonProperty("enterprise_type")] public EnterpriseTypeOutput EnterpriseType { get; set; }
        [JsonProperty("email_enterprise")] public string Email { get; set; }
        [JsonProperty("photo")] public string Photo { get; set; }
        [JsonProperty("facebook")] public string Facebook { get; set; }
        [JsonProperty("twitter")] public string Twitter { get; set; }
        [JsonProperty("linkedin")] public string Linkedin { get; set; }
        [JsonProperty("phone")] public string Phone { get; set; }
        [JsonProperty("own_enterprise")] public bool OwnEnterprise { get; set; }
        [JsonProperty("share_price")] public long SharePrice { get; set; }
    }
}
