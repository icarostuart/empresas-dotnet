﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Ioasys.Services.DTOs.Enterprises.Outputs
{
    public partial class EnterpriseIndexGroupOutput
    {
        [JsonProperty("enterprises")]
        public List<EnterpriseIndexOutput> Enterprises { get; set; }
    }
}
