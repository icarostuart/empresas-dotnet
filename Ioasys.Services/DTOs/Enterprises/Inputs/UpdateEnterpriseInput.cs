﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Ioasys.Services.DTOs.Enterprises.Inputs
{
    public partial class UpdateEnterpriseInput
    {
        [JsonProperty("id"), Required] public long Id { get; set; }
        [JsonProperty("enterprise_name"), Required] public string Name { get; set; }
        [JsonProperty("description"), Required] public string Description { get; set; }
        [JsonProperty("value"), Required] public long Value { get; set; }
        [JsonProperty("city"), Required] public string City { get; set; }
        [JsonProperty("country"), Required] public string Country { get; set; }
        [JsonProperty("enterprise_type_id"), Required] public int EnterpriseTypeId { get; set; }

        // optionals
        [JsonProperty("email_enterprise")] public string Email { get; set; }
        [JsonProperty("facebook")] public string Facebook { get; set; }
        [JsonProperty("twitter")] public string Twitter { get; set; }
        [JsonProperty("linkedin")] public string Linkedin { get; set; }
        [JsonProperty("phone")] public string Phone { get; set; }
        [JsonProperty("photo")] public string Photo { get; set; }
        [JsonProperty("own_enterprise")] public bool? OwnEnterprise { get; set; }
        [JsonProperty("shares")] public long? Shares { get; set; }
        [JsonProperty("share_price")] public long? SharePrice { get; set; }
        [JsonProperty("own_shares")] public long? OwnShares { get; set; }
    }
}
