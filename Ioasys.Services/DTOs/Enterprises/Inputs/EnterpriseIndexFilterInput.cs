﻿using Microsoft.AspNetCore.Mvc;

namespace Ioasys.Services.DTOs.Enterprises.Inputs
{
    public partial class EnterpriseIndexFilterInput
    {
        [FromQuery(Name = "id")] public long? Id { get; set; }
        [FromQuery(Name = "name")] public string Name { get; set; }
        [FromQuery(Name = "photo")] public string Photo { get; set; }
        [FromQuery(Name = "description")] public string Description { get; set; }
        [FromQuery(Name = "email_enterprise")] public string Email { get; set; }
        [FromQuery(Name = "facebook")] public string Facebook { get; set; }
        [FromQuery(Name = "twitter")] public string Twitter { get; set; }
        [FromQuery(Name = "linkedin")] public string Linkedin { get; set; }
        [FromQuery(Name = "phone")] public string Phone { get; set; }
        [FromQuery(Name = "own_enterprise")] public bool? OwnEnterprise { get; set; }
        [FromQuery(Name = "city")] public string City { get; set; }
        [FromQuery(Name = "country")] public string Country { get; set; }
        [FromQuery(Name = "value")] public long? Value { get; set; }
        [FromQuery(Name = "share_price")] public long? SharePrice { get; set; }
        [FromQuery(Name = "enterprise_types")] public int? EnterpriseTypeId { get; set; }
    }
}
