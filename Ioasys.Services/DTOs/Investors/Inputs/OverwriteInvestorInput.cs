﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ioasys.Services.DTOs.Investors.Inputs
{
    public partial class OverwriteInvestorInput
    {
        [JsonProperty("id"), Required] public long Id { get; set; }
        [Required, JsonProperty("investor_name")] public string Name { get; set; }
        [Required, JsonProperty("investor_email")] public string Email { get; set; }
        [Required, JsonProperty("city")] public string City { get; set; }
        [Required, JsonProperty("country")] public string Country { get; set; }
        [Required, JsonProperty("super_angel")] public bool SuperAngel { get; set; }

        // optionals
        [JsonProperty("enterprise_id")] public long? EnterpriseId { get; set; }
        [JsonProperty("photo")] public string Photo { get; set; }
        [JsonProperty("balance")] public long? Balance { get; set; }
        [JsonProperty("portfolio")] public HashSet<long> Portfolio { get; set; } = new HashSet<long>();
    }
}
