﻿using Ioasys.Domain.Entities;
using Ioasys.Domain.Util;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ioasys.Services.DTOs.Investors.Inputs
{
    public partial class UpdateInvestorInput
    {
        [JsonProperty("investor_name")] public string Name { get; set; }
        [JsonProperty("investor_email")] public UpdateEmailInput Email { get; set; }
        [JsonProperty("location")] public UpdateLocationInput Location { get; set; }
        [JsonProperty("super_angel")] public bool SuperAngel { get; set; }
        [JsonProperty("photo")] public string Photo { get; set; }
        [JsonProperty("balance")] public long? Balance { get; set; }
        [JsonProperty("investors_enterprises")] public virtual HashSet<UpdateInvestorEnterpriseInput> InvestorsEnterprises { get; set; }
        [JsonProperty("enterprise_id")] public long? EnterpriseId { get; set; }
    }

    public class UpdateEmailInput
    {
        public UpdateEmailInput(string address)
        {
            if (!string.IsNullOrWhiteSpace(address))
            {
                address.ValidateEmail();
            }

            Address = address;
        }

        [JsonProperty("address")] public string Address { get; set; }
    }

    public class UpdateLocationInput
    {
        public UpdateLocationInput(string city, string country)
        {
            City = city;
            Country = country;
        }

        [JsonProperty("city")] public string City { get; set; }
        [JsonProperty("country")] public string Country { get; set; }
    }

    public class UpdateInvestorEnterpriseInput
    {
        [JsonProperty("investor_id")] public long InvestorId { get; set; }
        [JsonProperty("enterprise_id")] public long EnterpriseId { get; set; }
    }
}
