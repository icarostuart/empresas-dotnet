﻿using Ioasys.Domain.Entities;
using Ioasys.Services.DTOs.Enterprises.Outputs;
using Newtonsoft.Json;
using System.Linq;

namespace Ioasys.Services.DTOs.Investors.Outputs
{
    public partial class InvestorOutput
    {
        public InvestorOutput() { }

        public InvestorOutput(Investor investor)
        {
            Id = investor.Id;
            Name = investor.Name;
            Email = investor.Email.Address;
            Photo = investor.Photo;
            City = investor.Location.City;
            Country = investor.Location.Country;
            Balance = investor.Balance;
            PortfolioValue = investor.PortfolioValue;
            FirstAccess = investor.FirstAccess;
            SuperAngel = investor.SuperAngel;
            Portfolio = new PortfolioOutput
            {
                Enterprises = investor.Enterprises.Select(e => new EnterpriseDetailOutput(e)).ToList()
            };
        }

        [JsonProperty("id")] public long Id { get; set; }
        [JsonProperty("investor_name")] public string Name { get; set; }
        [JsonProperty("investor_email")] public string Email { get; set; }
        [JsonProperty("city")] public string City { get; set; }
        [JsonProperty("country")]  public string Country { get; set; }
        [JsonProperty("portfolio_value")] public long PortfolioValue { get; set; }
        [JsonProperty("first_access")] public bool? FirstAccess { get; set; }
        [JsonProperty("super_angel")] public bool SuperAngel { get; set; }
        [JsonProperty("balance")] public long Balance { get; set; }
        [JsonProperty("photo")] public string Photo { get; set; }
        [JsonProperty("portfolio")] public PortfolioOutput Portfolio { get; set; }
    }
}
