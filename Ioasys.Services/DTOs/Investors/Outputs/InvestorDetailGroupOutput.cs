﻿using Newtonsoft.Json;

namespace Ioasys.Services.DTOs.Investors.Outputs
{
    public partial class InvestorDetailGroupOutput
    {
        [JsonProperty("investor")] public InvestorOutput Investor { get; set; }
        [JsonProperty("success")] public bool Success => true;
    }
}
