﻿using Ioasys.Services.DTOs.Enterprises.Outputs;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Ioasys.Services.DTOs.Investors.Outputs
{
    public partial class PortfolioOutput
    {
        [JsonProperty("enterprises_number")]
        public long Count => Enterprises.Count;

        [JsonProperty("enterprises")]
        public List<EnterpriseDetailOutput> Enterprises { get; set; }
    }
}
