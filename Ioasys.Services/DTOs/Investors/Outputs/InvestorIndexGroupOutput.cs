﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Ioasys.Services.DTOs.Investors.Outputs
{
    public partial class InvestorIndexGroupOutput
    {
        [JsonProperty("investors")]
        public List<InvestorOutput> Investors { get; set; }
    }
}
