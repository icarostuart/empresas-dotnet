﻿using AutoMapper;
using Ioasys.Domain.Entities;
using Ioasys.Services.DTOs.Auth.Outputs;
using Ioasys.Services.DTOs.Enterprises.Outputs;
using Ioasys.Services.DTOs.Investors.Outputs;
using System.Collections.Generic;
using System.Linq;

namespace Ioasys.Services.MapperFactories
{
    public class DomainToOutputProfile : Profile
    {
        public DomainToOutputProfile()
        {
            CreateMap<Enterprise, EnterpriseDetailGroupOutput>()
                .ForMember(dest => dest.Enterprise,
                           opts => opts.MapFrom(src => new EnterpriseDetailOutput(src)));

            CreateMap<List<Enterprise>, EnterpriseIndexGroupOutput>()
                .ForMember(dest => dest.Enterprises,
                           opts => opts.MapFrom(src => src.Select(x => new EnterpriseIndexOutput(x)).ToList()));

            CreateMap<Investor, InvestorDetailGroupOutput>()
                .ForMember(dest => dest.Investor,
                           opts => opts.MapFrom(src => new InvestorOutput(src)));

            CreateMap<List<Investor>, InvestorIndexGroupOutput>()
                .ForMember(dest => dest.Investors,
                           opts => opts.MapFrom(src => src.Select(x => new InvestorOutput(x)).ToList()));

            CreateMap<Investor, InvestorOutput>();

            CreateMap<EnterpriseType, EnterpriseTypeOutput>();

            CreateMap<Investor, SuccessSigninOutput>()
                .ForMember(dest => dest.Investor, opts => opts.MapFrom(src => new InvestorOutput(src)));

            CreateMap<List<Enterprise>, PortfolioOutput>()
                .ForMember(dest => dest.Enterprises, opts => opts.MapFrom(src => src.Select(x => new EnterpriseDetailOutput(x)).ToList()));
        }
    }
}
