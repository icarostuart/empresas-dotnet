﻿using AutoMapper;
using Ioasys.Domain.ComplexTypes;
using Ioasys.Domain.Entities;
using Ioasys.Domain.Repositories;
using Ioasys.Domain.Unities;
using Ioasys.Domain.Util;
using Ioasys.Services.DTOs.Enterprises.Inputs;
using Ioasys.Services.DTOs.Investors.Inputs;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.JsonPatch.Operations;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Ioasys.Services.MapperFactories
{
    public class InputToDomainProfile : Profile
    {
        private readonly IInvestorRepository investors;
        private readonly IEnterpriseRepository enterprises;
        private readonly IEnterpriseTypeRepository types;

        public InputToDomainProfile(IUnitOfEnterprises uow)
        {
            enterprises = uow.Enterprises;
            investors = uow.Investors;
            types = uow.Types;

            CreateMap<Email, string>().ConvertUsing(src => src != null ? src.Address : null);
            CreateMap<string, Email>().ConvertUsing(src => new Email(src));

            CreateMap<EnterpriseIndexFilterInput, Enterprise>()
                .ForMember(dest => dest.Location, opts => opts.MapFrom(src => new Location(src.City, src.Country)))
                .ForMember(dest => dest.Contact, opts => opts.MapFrom(src => new Contact(src.Phone, src.Facebook, src.Twitter, src.Linkedin)));

            CreateMap<CreateEnterpriseInput, Enterprise>()
                .ForMember(dest => dest.Location, opts => opts.MapFrom(src => new Location(src.City, src.Country)))
                .ForMember(dest => dest.Contact, opts => opts.MapFrom(src => new Contact(src.Phone, src.Facebook, src.Twitter, src.Linkedin)));

            CreateMap<UpdateEnterpriseInput, Enterprise>()
                .ForMember(dest => dest.Location, opts => opts.MapFrom(src => new Location(src.City, src.Country)))
                .ForMember(dest => dest.Contact, opts => opts.MapFrom(src => new Contact(src.Phone, src.Facebook, src.Twitter, src.Linkedin)));

            CreateMap<long, Enterprise>().ConvertUsing((id, enterprise) =>
            {
                enterprise = enterprises.GetWithKeys(id) ?? throw new ApiException(HttpStatusCode.NotFound, "Not Found");
                enterprise = enterprises.LoadNavigation(enterprise, e => e.EnterpriseType);
                enterprise = enterprises.LoadCollection(enterprise, nameof(Investor.InvestorsEnterprises));

                return enterprise;
            });


            CreateMap<InvestorIndexFilterInput, Investor>()
                .ForMember(dest => dest.Location, opts => opts.MapFrom(src => new Location(src.City, src.Country)));

            CreateMap<CreateInvestorInput, Investor>()
                .IgnoreAllPropertiesWithAnInaccessibleSetter()
                .ForMember(dest => dest.Location, opts => opts.MapFrom(src => new Location(src.City, src.Country)))
                .ForMember(investor => investor.InvestorsEnterprises,
                               opts => opts.MapFrom((input, investor) =>
                               {
                                   var enterprisesQuery = enterprises.GetAll(included: $"{nameof(Enterprise.EnterpriseType)},{nameof(Enterprise.InvestorsEnterprises)}");

                                   input.Portfolio.Join(
                                       enterprisesQuery,
                                       enterpriseId => enterpriseId,
                                       enterprise => enterprise.Id,
                                       (id, enterprise) =>
                                       {
                                           investor.InvestorsEnterprises.Add(new InvestorEnterprise
                                           {
                                               Investor = investor,
                                               Enterprise = enterprise
                                           });

                                           return enterprise;
                                       });

                                   investor.UpdatePortfolioValue();

                                   return investor.InvestorsEnterprises;
                               }));

            CreateMap<OverwriteInvestorInput, Investor>()
                .IgnoreAllPropertiesWithAnInaccessibleSetter()
                .ForMember(dest => dest.Location, opts => opts.MapFrom(src => new Location(src.City, src.Country)))
                .ForMember(investor => investor.InvestorsEnterprises,
                               opts => opts.MapFrom((input, investor) =>
                               {
                                   var enterprisesQuery = enterprises.GetAll(included: $"{nameof(Enterprise.EnterpriseType)},{nameof(Enterprise.InvestorsEnterprises)}");

                                   input.Portfolio.Join(
                                       enterprisesQuery,
                                       enterpriseId => enterpriseId,
                                       enterprise => enterprise.Id,
                                       (id, enterprise) =>
                                       {
                                           investor.InvestorsEnterprises.Add(new InvestorEnterprise
                                           {
                                               Investor = investor,
                                               Enterprise = enterprise
                                           });

                                           return enterprise;
                                       });

                                   investor.UpdatePortfolioValue();

                                   return investor.InvestorsEnterprises;
                               }));

            CreateMap<long, Investor>().ConvertUsing((id, investor) => investors
                .Query(included: $"{nameof(Investor.Enterprise)}" +
                                 $",{nameof(Investor.InvestorsEnterprises)}" +
                                 $",{nameof(Investor.InvestorsEnterprises)}.{nameof(InvestorEnterprise.Enterprise)}" +
                                 $",{nameof(Investor.InvestorsEnterprises)}.{nameof(InvestorEnterprise.Enterprise)}.{nameof(Enterprise.EnterpriseType)}")
                .FirstOrDefault(i => i.Id == id) ?? throw new ApiException(HttpStatusCode.NotFound, "Not Found"));

            CreateMap<UpdateEmailInput, Email>();
            CreateMap<UpdateLocationInput, Location>();
            CreateMap<HashSet<UpdateInvestorEnterpriseInput>, ICollection<InvestorEnterprise>>();
            CreateMap<JsonPatchDocument<UpdateInvestorInput>, JsonPatchDocument<Investor>>();
            CreateMap<Operation<OverwriteInvestorInput>, Operation<Investor>>();
        }
}
}
